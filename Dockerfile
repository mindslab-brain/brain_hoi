FROM pytorch/pytorch:1.7.1-cuda11.0-cudnn8-runtime 
ENV LANG ko_KR.UTF-8
ENV LANGUAGE ko_KR.UTF-8

RUN apt-get update 
RUN apt-get install -y --no-install-recommends \
    ca-certificates curl sudo gnupg wget tmux \
    htop rsync ncdu make git unzip bzip2 xz-utils vim openssl build-essential \
    libgl1-mesa-dev libglib2.0-0 grpcio grpcio-tools

RUN pip install --upgrade pip setuptools wheel

RUN pip install torchvision==0.8.2 timm \
    gdown==3.13.0 tqdm==4.60.0 \
    numpy==1.19.5 opencv_python==4.5.1.48 matplotlib==3.3.4 \
    seaborn==0.11.1 scikit_image==0.17.2 \
    Pillow==8.4.0 scikit_learn jupyter

RUN pip install scipy cython
RUN pip install pycocotools

RUN bash build_proto.sh
RUN bash setup.sh
