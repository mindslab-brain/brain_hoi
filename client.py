r'''
    HOI Client sample
    Author: Sungguk Cha
    eMail:  sungguk@mindslab.ai
    Date: 21 Feb. 2022
'''

import os
import sys
import time

from proto.hoi_pb2 import HOIInput
from proto.hoi_pb2_grpc import HOIStub

import grpc
import argparse

CHUNK_SIZE = 1024 * 1024  # 1MB

class Client(object):
    def __init__(self, ip="localhost", port=5959):
        self.server_ip = ip
        self.server_port = port
        self.stub = HOIStub(
            grpc.insecure_channel(
                self.server_ip + ":" + str(self.server_port)
            )
        )

    def get_service(self, image_byte):
        binary_iterator = self._generate_binary_iterator(image_byte)

        before = time.time()
        output = self.stub.Determinate(binary_iterator)
        after = time.time()
        passed = after - before

        text = output.text
        answers = text.split(',')
        
        print(answers)
        print(passed, "seconds passed")

    @staticmethod
    def _generate_binary_iterator(image_binary):
        for idx in range(0, len(image_binary), CHUNK_SIZE):
            yield HOIInput(image=image_binary[idx:idx+CHUNK_SIZE])

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='HOI Client')
    parser.add_argument('--ip',
                        type=str,
                        help="HOI server ip (default: localhost)",
                        default="0.0.0.0")
    parser.add_argument('--port',
                        type=int,
                        default=5959)
    args = parser.parse_args()

    image_path = '2022-02-15 12_12_26.080028.png'
    image_path = os.path.join("/DATA2/vision/data/call", image_path)

    with open(image_path, 'rb') as rf:
        image_byte = rf.read()

    client = Client(ip=args.ip, port=args.port)
    client.get_service(image_byte)
