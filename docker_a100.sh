#!/bin/bash

PROJECT_NAME="brain_hoi"

cpuset0='48-63'
cpuset1='176-191'
cpuset01='48-63,176-191'
cpuset2='16-31'
cpuset23='16-31,144-159'
cpuset45='112-127,240-255'
cpuset67='80-95,208-223'
cpuset7="208-223"

IMAGE="docker.maum.ai:443/brain/vision:v0.3.0-cu110"
CONT_NAME=${PROJECT_NAME}

GPUS="2,3"
cpuset=$cpuset23

HOME_DIR="/home/sungguk"
PROJECT_DIR="${HOME_DIR}/projects/${PROJECT_NAME}"
DATA_DIR="/DATA2/vision/"

CMD="bash"

echo "kill container if exists"
docker container kill ${CONT_NAME}
docker container rm ${CONT_NAME}

echo "Running a container using GPU(s): ${GPUS}"
docker run -it -d \
--name "${CONT_NAME}" \
--gpus "\"device=${GPUS}\"" \
--cpuset-cpus="${cpuset}" \
-v "${PROJECT_DIR}:/root/${PROJECT_NAME}" \
-v "${DATA_DIR}:${DATA_DIR}" \
--net=host \
--ipc=host \
${IMAGE} ${CMD};

