IMAGE="brain_hoi:v0.2"
CONT_NAME="brain_hoi"
 
GPUS="2,3"
 
HOME_DIR="/home/sungguk"
PROJECT_NAME="brain_hoi"
PROJECT_DIR="${HOME_DIR}/projects/${PROJECT_NAME}"
DATA_DIR="/DATA2/vision/"

CUDA_HOME="/usr/local/cuda"
 
CMD="bash"

echo "kill and remove container if one with the same name exists"
docker container kill $CONT_NAME
docker container rm $CONT_NAME

echo "Running a container using GPU(s): ${GPUS}"
docker run -it -d \
--name "${CONT_NAME}" \
--gpus "\"device=${GPUS}\"" \
-v "${PROJECT_DIR}:/root/${PROJECT_NAME}" \
-v "${DATA_DIR}:${DATA_DIR}" \
-v "${CUDA_HOME}:${CUDA_HOME}" \
--net=host \
--ipc=host \
${IMAGE} ${CMD};
