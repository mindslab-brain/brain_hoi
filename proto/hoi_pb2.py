# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: proto/hoi.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x0fproto/hoi.proto\x12\x0emaum.brain.hoi\"\x19\n\x08HOIInput\x12\r\n\x05image\x18\x01 \x01(\x0c\"\x19\n\tHOIOutput\x12\x0c\n\x04text\x18\x01 \x01(\t2K\n\x03HOI\x12\x44\n\x0b\x44\x65terminate\x12\x18.maum.brain.hoi.HOIInput\x1a\x19.maum.brain.hoi.HOIOutput(\x01\x62\x06proto3')



_HOIINPUT = DESCRIPTOR.message_types_by_name['HOIInput']
_HOIOUTPUT = DESCRIPTOR.message_types_by_name['HOIOutput']
HOIInput = _reflection.GeneratedProtocolMessageType('HOIInput', (_message.Message,), {
  'DESCRIPTOR' : _HOIINPUT,
  '__module__' : 'proto.hoi_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.hoi.HOIInput)
  })
_sym_db.RegisterMessage(HOIInput)

HOIOutput = _reflection.GeneratedProtocolMessageType('HOIOutput', (_message.Message,), {
  'DESCRIPTOR' : _HOIOUTPUT,
  '__module__' : 'proto.hoi_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.hoi.HOIOutput)
  })
_sym_db.RegisterMessage(HOIOutput)

_HOI = DESCRIPTOR.services_by_name['HOI']
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _HOIINPUT._serialized_start=35
  _HOIINPUT._serialized_end=60
  _HOIOUTPUT._serialized_start=62
  _HOIOUTPUT._serialized_end=87
  _HOI._serialized_start=89
  _HOI._serialized_end=164
# @@protoc_insertion_point(module_scope)
