r'''
    Author: Sungguk Cha
    eMail:  sungguk@mindslab.ai
    Date:   21 Feb. 2022
'''

from concurrent import futures
import sys
import time
import argparse
sys.path.append("./src/QAHOI")

import cv2
import grpc
import numpy as np
from omegaconf import OmegaConf
from PIL import Image

from proto.hoi_pb2 import HOIOutput
from proto.hoi_pb2_grpc import HOIServicer, add_HOIServicer_to_server
from src import get_model

ONE_DAY_IN_SECONDS = 60 * 60 * 24
CHUNK_SIZE = 1024 * 1024
MAX_SIZE = 4 * CHUNK_SIZE


class HOIServicerImpl(HOIServicer):
    def __init__(self, configs):
        self.model = get_model(configs)

    def Determinate(self, hoi_iterator, context):
        try:
            image_data = bytearray()

            for input_data in hoi_iterator:
                if input_data.image is not None and len(input_data.image) != 0:
                    image_data.extend(input_data.image)
                if len(image_data) > MAX_SIZE:
                    err = "input size error: image < 4MB, text < 1MB"
                    #self.logger.debug("input size error: image < 4MB, text < 1MB")
                    return self._generate_binary_iterator(err)

            image_data = bytes(image_data)
            #self.logger.info(f'request:{len(image_data)} & {len(bytes(text_data, encoding="utf-8"))} bytes')

            image_np = cv2.imdecode(np.frombuffer(image_data, dtype=np.uint8), cv2.IMREAD_COLOR)
            image = Image.fromarray(image_np)

            before = time.time()
            results = self.model.inference(image)
            #self.logger.debug(f'inference_time: {time.time() - before}')

            ans = ""
            for i, text in enumerate(results):
                if i > 0: ans += ","
                ans += text

            return self._generate_binary_iterator(ans)

        except AssertionError as e:
            #self.logger.exception(e)
            context.set_code(grpc.StatusCode.UNIMPLEMENTED)
            context.set_details(str(e))

        except Exception as e:
            #self.logger.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    @staticmethod
    def _generate_binary_iterator(binary):
        return HOIOutput(text=binary)

def main():
    parser = argparse.ArgumentParser(description='HOI Server')
    parser.add_argument('--config', default='./configs/default.yaml')
    parser.add_argument('--log_level',
                        type=str,
                        help='logger level (default: INFO)',
                        default='INFO')
    args = parser.parse_args()

    #logger = setup_logger(logger_name='Brain_HOI', level=args.log_level.upper())
    config = OmegaConf.load(args.config)
    config.port = 5959

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    service_server = HOIServicerImpl(config)

    """
    from PIL import Image
    import os
    image_path = '2022-02-15 12_12_26.080028.png'
    image_path = os.path.join("/DATA2/vision/data/call", image_path)
    image = Image.open(image_path)    
    print(service_server.model.inference(image))
    return
    """

    add_HOIServicer_to_server(service_server, server)
    server.add_insecure_port(f"[::]:{config.port}")
    server.start()

    print("Server runs")
    #logger.info("server started >> {}".format(config.port))
    try:
        while True:
            time.sleep(ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    main()
