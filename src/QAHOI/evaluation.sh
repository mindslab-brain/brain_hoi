param="/DATA2/vision/checkpoints/eyes/hoi/QAHOI/QAHOI_swin_large_384_22k_mul3.pth"
hoi_path="/DATA2/vision/data/hico_20160224_det"

python main.py --resume $param --backbone swin_large_384 --num_feature_levels 3 --use_nms --eval --hoi_path $hoi_path
