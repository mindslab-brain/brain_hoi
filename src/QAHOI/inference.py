# reference: https://gist.github.com/cjw2021/5629f018ba00b2bed9ca73b9887e9b91

import argparse
import sys
import os

from pathlib import Path

import numpy as np
import torch
from torch import nn
import torch.nn.functional as F

from util.box_ops import box_cxcywh_to_xyxy
from models.backbone import build_backbone
from models.deformable_transformer import build_deforamble_transformer
from models.QAHOI import QAHOI

import torchvision.transforms as T
from PIL import Image
import matplotlib.pyplot as plt


coco_class_dict = {
    1: 'person', 2: 'bicycle', 3: 'car', 4: 'motorcycle', 5: 'airplane', 
    6: 'bus', 7: 'train', 8: 'truck', 9: 'boat', 10: 'traffic light', 
    11: 'fire hydrant', 13: 'stop sign', 14: 'parking meter', 15: 'bench', 16: 'bird', 
    17: 'cat', 18: 'dog', 19: 'horse', 20: 'sheep', 21: 'cow', 
    22: 'elephant', 23: 'bear', 24: 'zebra', 25: 'giraffe', 27: 'backpack', 
    28: 'umbrella', 31: 'handbag', 32: 'tie', 33: 'suitcase', 34: 'frisbee', 
    35: 'skis', 36: 'snowboard', 37: 'sports ball', 38: 'kite', 39: 'baseball bat', 
    40: 'baseball glove', 41: 'skateboard', 42: 'surfboard', 43: 'tennis racket', 44: 'bottle', 
    46: 'wine glass', 47: 'cup', 48: 'fork', 49: 'knife', 50: 'spoon', 
    51: 'bowl', 52: 'banana', 53: 'apple', 54: 'sandwich', 55: 'orange', 
    56: 'broccoli', 57: 'carrot', 58: 'hot dog', 59: 'pizza', 60: 'donut', 
    61: 'cake', 62: 'chair', 63: 'couch', 64: 'potted plant', 65: 'bed', 
    67: 'dining table', 70: 'toilet', 72: 'tv', 73: 'laptop', 74: 'mouse', 
    75: 'remote', 76: 'keyboard', 77: 'cell phone', 78: 'microwave', 79: 'oven', 
    80: 'toaster', 81: 'sink', 82: 'refrigerator', 84: 'book', 85: 'clock', 
    86: 'vase', 87: 'scissors', 88: 'teddy bear', 89: 'hair drier', 90: 'toothbrush'
}

coco_class_list = list(coco_class_dict.values())

valid_obj_ids = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13,
                 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
                 24, 25, 27, 28, 31, 32, 33, 34, 35, 36,
                 37, 38, 39, 40, 41, 42, 43, 44, 46, 47,
                 48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
                 58, 59, 60, 61, 62, 63, 64, 65, 67, 70,
                 72, 73, 74, 75, 76, 77, 78, 79, 80, 81,
                 82, 84, 85, 86, 87, 88, 89, 90)
valid_verb_ids = list(range(1, 118))

hico_verb_dict = {
    1: 'adjust', 2: 'assemble', 3: 'block', 4: 'blow', 5: 'board', 6: 'break', 7: 'brush_with', 8: 'buy', 
    9: 'carry', 10: 'catch', 11: 'chase', 12: 'check', 13: 'clean', 14: 'control', 15: 'cook', 16: 'cut', 
    17: 'cut_with', 18: 'direct', 19: 'drag', 20: 'dribble', 21: 'drink_with', 22: 'drive', 23: 'dry', 24: 'eat', 
    25: 'eat_at', 26: 'exit', 27: 'feed', 28: 'fill', 29: 'flip', 30: 'flush', 31: 'fly', 32: 'greet', 
    33: 'grind', 34: 'groom', 35: 'herd', 36: 'hit', 37: 'hold', 38: 'hop_on', 39: 'hose', 40: 'hug', 
    41: 'hunt', 42: 'inspect', 43: 'install', 44: 'jump', 45: 'kick', 46: 'kiss', 47: 'lasso', 48: 'launch', 
    49: 'lick', 50: 'lie_on', 51: 'lift', 52: 'light', 53: 'load', 54: 'lose', 55: 'make', 56: 'milk', 
    57: 'move', 58: 'no_interaction', 59: 'open', 60: 'operate', 61: 'pack', 62: 'paint', 63: 'park', 64: 'pay', 
    65: 'peel', 66: 'pet', 67: 'pick', 68: 'pick_up', 69: 'point', 70: 'pour', 71: 'pull', 72: 'push', 
    73: 'race', 74: 'read', 75: 'release', 76: 'repair', 77: 'ride', 78: 'row', 79: 'run', 80: 'sail', 
    81: 'scratch', 82: 'serve', 83: 'set', 84: 'shear', 85: 'sign', 86: 'sip', 87: 'sit_at', 88: 'sit_on', 
    89: 'slide', 90: 'smell', 91: 'spin', 92: 'squeeze', 93: 'stab', 94: 'stand_on', 95: 'stand_under', 96: 'stick', 
    97: 'stir', 98: 'stop_at', 99: 'straddle', 100: 'swing', 101: 'tag', 102: 'talk_on', 103: 'teach', 104: 'text_on', 
    105: 'throw', 106: 'tie', 107: 'toast', 108: 'train', 109: 'turn', 110: 'type_on', 111: 'walk', 112: 'wash', 
    113: 'watch', 114: 'wave', 115: 'wear', 116: 'wield', 117: 'zip'
}

COLORS = [[0.000, 0.447, 0.741], [0.850, 0.325, 0.098], [0.929, 0.694, 0.125],
          [0.494, 0.184, 0.556], [0.466, 0.674, 0.188], [0.301, 0.745, 0.933],
          [0.741, 0.447, 0.000], [0.098, 0.325, 0.850], [0.125, 0.929, 0.694],
          [0.188, 0.674, 0.466]]

class PostProcessHOI(nn.Module):

    def __init__(self, subject_category_id):
        super().__init__()
        self.subject_category_id = subject_category_id

    @torch.no_grad()
    def forward(self, outputs, target_sizes):
        out_obj_logits, out_verb_logits, out_sub_boxes, out_obj_boxes = outputs['pred_obj_logits'], \
                                                                        outputs['pred_verb_logits'], \
                                                                        outputs['pred_sub_boxes'], \
                                                                        outputs['pred_obj_boxes']



        assert len(out_obj_logits) == len(target_sizes)
        assert target_sizes.shape[1] == 2

        obj_prob = F.softmax(out_obj_logits, -1)
        verb_scores = out_verb_logits.sigmoid()
        
        # top 100
        topk_values, topk_indexes = torch.topk(obj_prob.view(out_obj_logits.shape[0], -1), 100, dim=1)
        obj_scores = topk_values
        topk_boxes = topk_indexes // out_obj_logits.shape[2]
        obj_labels = topk_indexes % out_obj_logits.shape[2]

        # top 100
        verb_scores = torch.gather(verb_scores, 1, topk_boxes.unsqueeze(-1).repeat(1,1,117))
        out_obj_boxes = torch.gather(out_obj_boxes, 1, topk_boxes.unsqueeze(-1).repeat(1,1,4))
        out_sub_boxes = torch.gather(out_sub_boxes, 1, topk_boxes.unsqueeze(-1).repeat(1,1,4))

        img_h, img_w = target_sizes.unbind(1)
        scale_fct = torch.stack([img_w, img_h, img_w, img_h], dim=1).to(verb_scores.device)
        sub_boxes = box_cxcywh_to_xyxy(out_sub_boxes)
        obj_boxes = box_cxcywh_to_xyxy(out_obj_boxes)
        
        sub_boxes = sub_boxes * scale_fct[:, None, :]
        obj_boxes = obj_boxes * scale_fct[:, None, :]

        results = []
        for os, ol, vs, sb, ob in zip(obj_scores, obj_labels, verb_scores, sub_boxes, obj_boxes):
        
            sl = torch.full_like(ol, self.subject_category_id)
            l = torch.cat((sl, ol))
            b = torch.cat((sb, ob))
            results.append({'labels': l.to('cpu'), 'boxes': b.to('cpu')})

            vs = vs * os.unsqueeze(1)

            ids = torch.arange(b.shape[0])

            results[-1].update({'verb_scores': vs.to('cpu'), 'sub_ids': ids[:ids.shape[0] // 2],
                                'obj_ids': ids[ids.shape[0] // 2:]})

        return results

def pairwise_nms(subs, objs, scores, nms_thresh=0.5):
    sx1, sy1, sx2, sy2 = subs[:, 0], subs[:, 1], subs[:, 2], subs[:, 3]
    ox1, oy1, ox2, oy2 = objs[:, 0], objs[:, 1], objs[:, 2], objs[:, 3]

    sub_areas = (sx2 - sx1 + 1) * (sy2 - sy1 + 1)
    obj_areas = (ox2 - ox1 + 1) * (oy2 - oy1 + 1)

    order = scores.argsort()[::-1]

    keep_inds = []
    while order.size > 0:
        i = order[0]
        keep_inds.append(i)

        sxx1 = np.maximum(sx1[i], sx1[order[1:]])
        syy1 = np.maximum(sy1[i], sy1[order[1:]])
        sxx2 = np.minimum(sx2[i], sx2[order[1:]])
        syy2 = np.minimum(sy2[i], sy2[order[1:]])

        sw = np.maximum(0.0, sxx2 - sxx1 + 1)
        sh = np.maximum(0.0, syy2 - syy1 + 1)
        sub_inter = sw * sh
        sub_union = sub_areas[i] + sub_areas[order[1:]] - sub_inter

        oxx1 = np.maximum(ox1[i], ox1[order[1:]])
        oyy1 = np.maximum(oy1[i], oy1[order[1:]])
        oxx2 = np.minimum(ox2[i], ox2[order[1:]])
        oyy2 = np.minimum(oy2[i], oy2[order[1:]])

        ow = np.maximum(0.0, oxx2 - oxx1 + 1)
        oh = np.maximum(0.0, oyy2 - oyy1 + 1)
        obj_inter = ow * oh
        obj_union = obj_areas[i] + obj_areas[order[1:]] - obj_inter

        ovr = sub_inter/sub_union * obj_inter/obj_union
        inds = np.where(ovr <= nms_thresh)[0]

        order = order[inds + 1]
    return keep_inds

def triplet_nms_filter(preds):
    preds_filtered = []
    for img_preds in preds:
        pred_bboxes = img_preds['predictions']
        pred_hois = img_preds['hoi_prediction']
        all_triplets = {}
        for index, pred_hoi in enumerate(pred_hois):
            triplet = str(pred_bboxes[pred_hoi['subject_id']]['category_id']) + '_' + \
                        str(pred_bboxes[pred_hoi['object_id']]['category_id']) + '_' + str(pred_hoi['category_id'])

            if triplet not in all_triplets:
                all_triplets[triplet] = {'subs':[], 'objs':[], 'scores':[], 'indexes':[]}
            all_triplets[triplet]['subs'].append(pred_bboxes[pred_hoi['subject_id']]['bbox'])
            all_triplets[triplet]['objs'].append(pred_bboxes[pred_hoi['object_id']]['bbox'])
            all_triplets[triplet]['scores'].append(pred_hoi['score'])
            all_triplets[triplet]['indexes'].append(index)

        all_keep_inds = []
        for triplet, values in all_triplets.items():
            subs, objs, scores = values['subs'], values['objs'], values['scores']
            keep_inds = pairwise_nms(np.array(subs), np.array(objs), np.array(scores))

            keep_inds = list(np.array(values['indexes'])[keep_inds])
            all_keep_inds.extend(keep_inds)

        preds_filtered.append({
            'filename': img_preds['filename'],
            'predictions': pred_bboxes,
            'hoi_prediction': list(np.array(img_preds['hoi_prediction'])[all_keep_inds])
            })

    return preds_filtered

def prepare_results(preds, correct_mat, max_hois=100):
    preds_t = []
    for img_preds in preds:
        img_preds = {k: v.to('cpu').numpy() for k, v in img_preds.items()}
        bboxes = [{'bbox': bbox, 'category_id': valid_obj_ids[label]} for bbox, label in zip(img_preds['boxes'], img_preds['labels'])]
        hoi_scores = img_preds['verb_scores']
        verb_labels = np.tile(np.arange(hoi_scores.shape[1]), (hoi_scores.shape[0], 1))
        subject_ids = np.tile(img_preds['sub_ids'], (hoi_scores.shape[1], 1)).T
        object_ids = np.tile(img_preds['obj_ids'], (hoi_scores.shape[1], 1)).T

        hoi_scores = hoi_scores.ravel()
        verb_labels = verb_labels.ravel()
        subject_ids = subject_ids.ravel()
        object_ids = object_ids.ravel()

        if len(subject_ids) > 0:
            object_labels = np.array([valid_obj_ids.index(bboxes[object_id]['category_id']) for object_id in object_ids])
            masks = correct_mat[verb_labels, object_labels]
            hoi_scores *= masks

            hois = [{'subject_id': subject_id, 'object_id': object_id, 'category_id': valid_verb_ids[category_id], 'score': score} for
                    subject_id, object_id, category_id, score in zip(subject_ids, object_ids, verb_labels, hoi_scores)]
            hois.sort(key=lambda k: (k.get('score', 0)), reverse=True)
            hois = hois[:max_hois]
        else:
            hois = []
        
        preds_t.append({
            'filename':"",
            'predictions': bboxes,
            'hoi_prediction': hois
        })

    preds_t = triplet_nms_filter(preds_t)
    return preds_t

def plot_results(pil_img, sub_boxes, obj_boxes, hoi_categories):
    plt.figure(figsize=(16,10))
    plt.imshow(pil_img)
    ax = plt.gca()
    colors = COLORS * 100
    for cat, sub, obj, c in zip(hoi_categories, sub_boxes, obj_boxes, colors):
        sub_box = sub['bbox']
        obj_box = obj['bbox']
        # sub_cat = sub['category_id']
        obj_cat = obj['category_id']
        ax.add_patch(plt.Rectangle((sub_box[0], sub_box[1]), sub_box[2] - sub_box[0], sub_box[3] - sub_box[1],
                                   fill=False, color=c, linewidth=3))
        ax.add_patch(plt.Rectangle((obj_box[0], obj_box[1]), obj_box[2] - obj_box[0], obj_box[3] - sub_box[1], fill=False, color=c, linewidth=3))
        sub_center = (sub_box[:2] + sub_box[2:]) / 2.0
        obj_center = (obj_box[:2] + obj_box[2:]) / 2.0
        ax.add_patch(plt.Arrow(sub_center[0], sub_center[1], obj_center[0] - sub_center[0], obj_center[1] - sub_center[1], color=c, linewidth=3))
        obj_text = coco_class_dict[obj_cat]

        ax.text(obj_box[0], obj_box[1], obj_text, fontsize=15,
                bbox=dict(facecolor='yellow', alpha=0.5))
        ax.text(sub_box[0], sub_box[1], hico_verb_dict[cat], fontsize=15, bbox=dict(facecolor='yellow', alpha=0.5))
    plt.axis('off')
    plt.savefig('result.png')

def get_args_parser():
    parser = argparse.ArgumentParser('QAHOI', add_help=False)

    parser.add_argument('--image', default='test.jpg', type=str,
                        help="image path")
    parser.add_argument('--corre_hico_path', default='./data/hico_20160224_det/annotations/', type=str,
                        help="image path")
    parser.add_argument('--top', default=5, type=int)

    
    parser.add_argument('--lr', default=1e-4, type=float)
    parser.add_argument('--lr_backbone_names', default=["backbone.0"], type=str, nargs='+')
    parser.add_argument('--lr_backbone', default=1e-5, type=float)
    parser.add_argument('--lr_linear_proj_names', default=['reference_points', 'sampling_offsets'], type=str, nargs='+')
    parser.add_argument('--lr_linear_proj_mult', default=1.0, type=float)
    parser.add_argument('--batch_size', default=2, type=int)
    parser.add_argument('--weight_decay', default=1e-4, type=float)
    parser.add_argument('--epochs', default=150, type=int)
    parser.add_argument('--lr_drop', default=120, type=int)
    parser.add_argument('--lr_drop_epochs', default=None, type=int, nargs='+')
    parser.add_argument('--clip_max_norm', default=0.1, type=float,
                        help='gradient clipping max norm')
    parser.add_argument('--nms_thresh', default=0.5, type=float)
    parser.add_argument('--use_checkpoint', action='store_true')

    # Model parameters
    parser.add_argument('--frozen_weights', type=str, default=None,
                        help="Path to the pretrained model. If set, only the mask head will be trained")
    parser.add_argument('--two_stage', action='store_true')

    # * Backbone
    parser.add_argument('--backbone', default='resnet50', type=str,
                        help="Name of the convolutional backbone to use")
    parser.add_argument('--dilation', action='store_true',
                        help="If true, we replace stride with dilation in the last convolutional block (DC5)")
    parser.add_argument('--position_embedding', default='sine', type=str, choices=('sine', 'learned'),
                        help="Type of positional embedding to use on top of the image features")
    parser.add_argument('--position_embedding_scale', default=2 * np.pi, type=float,
                        help="position / size * scale")
    parser.add_argument('--num_feature_levels', default=4, type=int, help='number of feature levels')

    # * Transformer
    parser.add_argument('--enc_layers', default=6, type=int,
                        help="Number of encoding layers in the transformer")
    parser.add_argument('--dec_layers', default=6, type=int,
                        help="Number of decoding layers in the transformer")
    parser.add_argument('--dim_feedforward', default=1024, type=int,
                        help="Intermediate size of the feedforward layers in the transformer blocks")
    parser.add_argument('--hidden_dim', default=256, type=int,
                        help="Size of the embeddings (dimension of the transformer)")
    parser.add_argument('--dropout', default=0.1, type=float,
                        help="Dropout applied in the transformer")
    parser.add_argument('--nheads', default=8, type=int,
                        help="Number of attention heads inside the transformer's attentions")
    parser.add_argument('--num_queries', default=300, type=int,
                        help="Number of query slots")
    parser.add_argument('--dec_n_points', default=4, type=int)
    parser.add_argument('--enc_n_points', default=4, type=int)

    # * Segmentation
    parser.add_argument('--masks', action='store_true',
                        help="Train segmentation head if the flag is provided")

    # HOI
    parser.add_argument('--pretrained', type=str, default="",
                        help='Pretrained model path')
    parser.add_argument('--num_verb_classes', type=int, default=117,
                        help="Number of verb classes")
    parser.add_argument('--num_obj_classes', type=int, default=80,
                        help="Number of object classes")
    parser.add_argument('--subject_category_id', default=0, type=int)

    # Loss
    parser.add_argument('--no_aux_loss', dest='aux_loss', action='store_false',
                        help="Disables auxiliary decoding losses (loss at each layer)")

    # * Matcher
    parser.add_argument('--set_cost_class', default=1, type=float,
                        help="Class coefficient in the matching cost")
    parser.add_argument('--set_cost_bbox', default=2.5, type=float,
                        help="L1 box coefficient in the matching cost")
    parser.add_argument('--set_cost_giou', default=1, type=float,
                        help="giou box coefficient in the matching cost")
    parser.add_argument('--set_cost_obj_class', default=1, type=float,
                        help="Object class coefficient in the matching cost")
    parser.add_argument('--set_cost_verb_class', default=1, type=float,
                        help="Verb class coefficient in the matching cost")

    # * Loss coefficients
    parser.add_argument('--mask_loss_coef', default=1, type=float)
    parser.add_argument('--dice_loss_coef', default=1, type=float)
    parser.add_argument('--bbox_loss_coef', default=2.5, type=float)
    parser.add_argument('--giou_loss_coef', default=1, type=float)
    parser.add_argument('--obj_loss_coef', default=1, type=float)
    parser.add_argument('--verb_loss_coef', default=1, type=float)
    parser.add_argument('--eos_coef', default=0.1, type=float,
                        help="Relative classification weight of the no-object class")

    # dataset parameters
    parser.add_argument('--dataset_file', default='hico')
    parser.add_argument('--coco_path', default='./data/coco', type=str)
    parser.add_argument('--coco_panoptic_path', type=str)
    parser.add_argument('--remove_difficult', action='store_true')
    parser.add_argument('--hoi_path', default='./data/hico_20160224_det', type=str)

    parser.add_argument('--output_dir', default='',
                        help='path where to save, empty for no saving')
    parser.add_argument('--device', default='cuda',
                        help='device to use for training / testing')
    parser.add_argument('--seed', default=42, type=int)
    parser.add_argument('--resume', default='', help='resume from checkpoint')
    parser.add_argument('--start_epoch', default=0, type=int, metavar='N',
                        help='start epoch')
    parser.add_argument('--eval', action='store_true')
    parser.add_argument('--eval_extra', action='store_true')
    parser.add_argument('--use_nms', action='store_true')
    parser.add_argument('--num_workers', default=2, type=int)

    return parser

def main(args):
    device = torch.device(args.device)

    backbone = build_backbone(args)

    transformer = build_deforamble_transformer(args)
    model = QAHOI(
        backbone,
        transformer,
        num_classes=args.num_obj_classes,
        num_verb_classes=args.num_verb_classes,
        num_queries=args.num_queries,
        num_feature_levels=args.num_feature_levels,
        aux_loss=False
    )

    checkpoint = torch.load(args.resume, map_location='cpu')
    model.load_state_dict(checkpoint['model'])

    model.to(device)
    model.eval()

    transform = T.Compose([
        T.Resize(800),
        T.ToTensor(),
        T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])

    im = Image.open(args.image)
    img = transform(im).unsqueeze(0).to(device)

    outputs = model(img)

    img_w, img_h = im.size
    target_sizes = torch.tensor([[img_h, img_w]]).to(device)

    postprocess = PostProcessHOI(args.subject_category_id)

    results = postprocess(outputs, target_sizes)

    correct_mat = np.load(os.path.join(args.corre_hico_path, 'corre_hico.npy'))
    preds = prepare_results(results, correct_mat)

    preds = preds[0]

    bboxes, hoi_predictions = preds['predictions'], preds['hoi_prediction']

    scores = np.array([pred['score'] for pred in hoi_predictions])
    score_sort_index = np.argsort(scores)[::-1]
    top_num = min(10, args.top)
    show_index = score_sort_index[:top_num].tolist()

    # bboxes = bboxes[show_index]
    # hoi_predictions = hoi_predictions[show_index]

    top_hoi_predictions = []

    index = 0
    for hoi_prediction in hoi_predictions:
        if index in show_index:
            top_hoi_predictions.append(hoi_prediction)
        index += 1

    sub_boxes = []
    obj_boxes = []
    hoi_categories = []

    for topi in top_hoi_predictions:
        sub_boxes.append(bboxes[topi['subject_id']])
        obj_boxes.append(bboxes[topi['object_id']])
        hoi_categories.append(topi['category_id'])

    plot_results(im, sub_boxes, obj_boxes, hoi_categories)


if __name__ == '__main__':
    parser = argparse.ArgumentParser('QAHOI Inference', parents=[get_args_parser()])
    args = parser.parse_args()
    if args.output_dir:
        Path(args.output_dir).mkdir(parents=True, exist_ok=True)
    main(args)
