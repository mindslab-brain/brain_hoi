r"""
    QAHOI model implementation for prediciton
    Author: Sungguk Cha
    eMail:  sungguk@mindslab.ai
    Date:   21 Feb. 2022
"""

import os
opj = os.path.join

import numpy as np
import torch
import torchvision.transforms as T

import src.QAHOI.inference as inference
from src.QAHOI.models.backbone import build_backbone
from src.QAHOI.models.deformable_transformer import build_deforamble_transformer
from src.QAHOI.models.QAHOI import QAHOI

class Args:
  image = 'test.jpg'
  corre_hico_path = "/DATA2/vision/data/hico_20160224_det/annotations"
  top = 5
  lr = 1e-4
  lr_backbone_names = ['backbone.0']
  lr_backbone = 1e-5
  lr_linear_proj_names = ['reference_points', 'sampling_offsets']
  lr_linear_proj_mult = 1.0
  batch_size = 2
  weight_decay = 1e-4
  epochs = 150
  lr_drop = 120
  lr_drop_epochs = None
  clip_max_norm = 0.1
  nms_thresh = 0.5
  use_checkpoint =  False
  frozen_weights = None
  two_stage =  False
  backbone = 'resnet50'
  dilation =  False
  position_embedding = 'sine'
  position_embedding_scale = 2 * np.pi
  num_feature_levels = 4
  enc_layers = 6
  dec_layers = 6
  dim_feedforward = 1024
  hidden_dim = 256
  dropout = 0.1
  nheads = 8
  num_queries = 300
  dec_n_points = 4
  enc_n_points = 4
  masks =  False
  pretrained = ''
  num_verb_classes = 117
  num_obj_classes = 80
  subject_category_id = 0
  no_aux_loss =  True
  set_cost_class = 1
  set_cost_bbox = 2.5
  set_cost_giou = 1
  set_cost_obj_class = 1
  set_cost_verb_class = 1
  mask_loss_coef = 1
  dice_loss_coef = 1
  bbox_loss_coef = 2.5
  giou_loss_coef = 1
  obj_loss_coef = 1
  verb_loss_coef = 1
  eos_coef = 0.1
  dataset_file = 'hico'
  coco_path = './data/coco'
  coco_panoptic_path =  ""
  remove_difficult =  False
  hoi_path = './data/hico_20160224_det'
  output_dir = ''
  device = 'cuda'
  seed = 42
  resume = ''
  start_epoch = 0
  eval =  False
  eval_extra =  False
  use_nms =  True
  num_workers = 2

transform = T.Compose([
    T.Resize(800),
    T.ToTensor(),
    T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])

class Model:
    def __init__(self, model, config):
        self.args = config
        self.model = model
        self.postprocess = inference.PostProcessHOI(subject_category_id=1)
        self.correct_mat = np.load(os.path.join(config.corre_hico_path, 'corre_hico.npy'))
        self.obj_dict = inference.coco_class_dict
        self.hoi_dict = inference.hico_verb_dict
    
    def inference(self, image):
        # image = ndarray
        image_T = transform(image).unsqueeze(0).cuda()
        outputs = self.model(image_T)
    
        w, h = image.size
        target_sizes = torch.tensor([[h, w]]).cuda()

        results = self.postprocess(outputs, target_sizes)
        preds = inference.prepare_results(results, self.correct_mat)

        pred = preds[0]
        bboxes, hoi_predictions = pred['predictions'], pred['hoi_prediction']

        scores = np.array([_pred['score'] for _pred in hoi_predictions])
        score_sort_index = np.argsort(scores)[::-1]
        top_num = min(10, self.args.top)
        show_index = score_sort_index[:top_num].tolist()

        # bboxes = bboxes[show_index]
        # hoi_predictions = hoi_predictions[show_index]

        top_hoi_predictions = []

        index = 0
        for hoi_prediction in hoi_predictions:
            if index in show_index:
                top_hoi_predictions.append(hoi_prediction)
            index += 1

        texts = list()
        for topi in top_hoi_predictions:
            obj_id = bboxes[topi['object_id']]['category_id']
            obj_str = self.obj_dict[obj_id]
            hoi_id = topi['category_id']
            hoi_str = self.hoi_dict[hoi_id]
            text = f"{hoi_str} {obj_str}"
            texts.append(text)

        return texts


def get_QAHOI(config):
    backbone = config.model.split('-')[-1]
    args = Args()
    args.backbone = backbone
    args.num_feature_levels = 3
    args.top = 5
    if backbone == "swin_large_384":
        args.resume = opj(config.checkpoint_dir, "QAHOI_swin_large_384_22k_mul3.pth")
    elif backbone == "swin_base_384":
        args.resume = opj(config.checkpoint_dir, "QAHOI_swin_base_384_22k_mul3.pth")
    elif backbone == "swin_tiny":
        args.resume = opj(config.checkpoint_dir, "QAHOI_swin_tiny_mul3.pth")
    else:
        raise NotImplementedError(f"backbone {backbone} is not supported")

    # load model
    backbone = build_backbone(args)

    transformer = build_deforamble_transformer(args)
    model = QAHOI(
        backbone,
        transformer,
        num_classes=args.num_obj_classes,
        num_verb_classes=args.num_verb_classes,
        num_queries=args.num_queries,
        num_feature_levels=args.num_feature_levels,
        aux_loss=False
    )

    checkpoint = torch.load(args.resume, map_location='cpu')
    model.load_state_dict(checkpoint['model'])

    model.cuda()
    model.eval()

    return Model(model, args)
