from src.QAHOI.qahoi import get_QAHOI

def get_model(config):
    if "qahoi" in config.model:
        ## requirements
        # checkpoint_dir
        # corre_hico_path and `corre_hico.npy` inside the path
        # model
        ## model options
        # qahoi-swin_large_384
        # qahoi-swin_base_384
        # qahoi-swin_tiny
        print("Loading QAHOI")
        return get_QAHOI(config)
    else:
        print(f"Not implemented error with model named {config.model}")
        return None